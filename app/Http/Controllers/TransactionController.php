<?php

namespace App\Http\Controllers;

use App\Http\Resources\TransactionResource;
use Carbon\Carbon;
use App\Models\Transaction;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $transaction = Transaction::orderBy('id')->get();
        $collection_transaction = TransactionResource::collection($transaction);

        return response()->json([
            'status_code' => 200,
            'data' => $collection_transaction
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => ['required'],
            'amount' => ['required', 'numeric'],
            'time' => ['required'],
            'type' => ['required', 'in:expense,revenue'],
            'image' => ['required', 'mimes:png,jpg,jpeg', 'max:2048'],
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(
                $validator->errors(),
                422
            );
        };

        $time = Carbon::createFromFormat('d-m-Y', $request->input('time'));

        $image = $request->file('image')->store('images');

        try {
            $transaction = new Transaction();
            $transaction->title = $request->title;
            $transaction->amount = $request->amount;
            $transaction->time = $time;
            $transaction->type = $request->type;
            $transaction->image = $image;
            $transaction->save();


            return response()->json([
                'status_code' => 200,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Gagal menambahkan data' . $e->errorInfo,
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $transaction = Transaction::find($id);
        $transaction_collection = new TransactionResource($transaction);


        if (!$transaction) {
            return response()->json([
                'status_code' => 404,
                'message' => 'Data tidak ditemukan',
            ], 404);
        }

        return response()->json([
            'status_code' => 200,
            'data' => $transaction_collection,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $transaction = Transaction::find($id);

        $rules = [
            'title' => ['required'],
            'amount' => ['required', 'numeric'],
            'time' => ['required'],
            'type' => ['required', 'in:expense,revenue'],
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(
                $validator->errors(),
                422
            );
        };

        if (!$transaction) {
            return response()->json([
                'status_code' => 404,
                'message' => 'Data tidak ditemukan',
            ], 404);
        }

        $time = Carbon::createFromFormat('d-m-Y', $request->input('time'));


        $transaction->update([
            'title' => $request->title,
            'amount' => $request->amount,
            'time' => $time,
            'type' => $request->type,
        ]);

        return response()->json([
            'status_code' => 200,
            'message' => 'Data berhasil diubah',
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $transaction = Transaction::find($id);

            if (!$transaction) {
                return response()->json([
                    'status_code' => 404,
                    'message' => 'Data tidak ditemukan',
                ], 404);
            }

            $transaction->delete();

            return response()->json([
                'status_code' => 200,
                'message' => 'data berhasil dihapus',
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Operasi gagal',
            ], 400);
        }
    }
}