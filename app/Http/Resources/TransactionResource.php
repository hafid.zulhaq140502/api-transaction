<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'amount' => $this->amount,
            'time' => Carbon::parse($this->time)->format('d-m-Y'),
            'custom_time' => Carbon::parse($this->time)->translatedFormat('d M Y'),
            'type' => $this->type,
            'image' => $this->image,
        ];
    }
}